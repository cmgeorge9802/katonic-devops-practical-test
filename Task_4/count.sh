#!/bin/bash


regions=(use1 use2 euc1)
environments=(staging qa uat prod)
for i in input.csv
do
	
	
	for region in ${regions[*]}
	do
		for environment in ${environments[*]}
		do
			echo "$region,$environment,$(grep -c "$region.$environment" ${i})"  
		done
	done	
	
done
		
