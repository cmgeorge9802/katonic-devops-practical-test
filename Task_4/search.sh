#!/usr/local/bin/bash

for file in input.csv
do
	use1_staging=$(grep "use1" ${file} | grep -c  "use1.staging")
	use1_qa=$(grep "use1" ${file} | grep -c "use1.qa")
	use1_prod=$(grep "use1" ${file} | grep -c "use1.prod")
	use1_uat=$(grep "use1" ${file} | grep -c "use1.uat")
done


for file in input.csv
do
        use2_staging=$(grep "use2" ${file} | grep -c  "use2.staging")
        use2_qa=$(grep "use2" ${file} | grep -c "use2.qa")
        use2_prod=$(grep "use2" ${file} | grep -c "use2.prod")
        use2_uat=$(grep "use2" ${file} | grep -c "use2.uat")
done


for file in input.csv
do
        euc1_staging=$(grep "euc1" ${file} | grep -c  "euc1.staging")
        euc1_qa=$(grep "euc1" ${file} | grep -c "euc1.qa")
        euc1_prod=$(grep "euc1" ${file} | grep -c "euc1.prod")
        euc1_uat=$(grep "euc1" ${file} | grep -c "euc1.uat")
done

echo "use1,staging,${use1_staging}"
echo "use1,qa,${use1_qa}"
echo "use1,uat,${use1_uat}"
echo "use1,prod,${use1_prod}"
echo "use2,staging,${use2_staging}"
echo "use2,qa,${use2_qa}"
echo "use2,uat,${use2_uat}"
echo "use2,prod,${use2_prod}"
echo "euc1,staging,${euc1_staging}"
echo "euc1,qa,${euc1_qa}"
echo "euc1,uat,${euc1_uat}"
echo "euc1,prod,${euc1_prod}"

